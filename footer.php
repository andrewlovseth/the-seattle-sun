	</main> <?php //.site-content ?>

	<footer class="site-footer grid">
		<?php get_template_part('template-parts/global/top-divider'); ?>

		<?php get_template_part('template-parts/footer/footer-content'); ?>

		<?php get_template_part('template-parts/global/bottom-divider'); ?>
	</footer>

<?php wp_footer(); ?>

</div> <?php //.site ?>

</body>
</html>