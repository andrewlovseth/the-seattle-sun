<?php

// Pull data from Google Spreadsheeet Source
function bearsmith_get_sheet_data($transient_name, $sheet_id, $tab_id, $timeout) {
    $url = "https://sheets.googleapis.com/v4/spreadsheets/" . $sheet_id . "/values/" . $tab_id . "!A2:Z?key=AIzaSyAJ1Hzo79gycFQGGSECizbUGo9aTW1uV5M";

    if(get_transient($transient_name)) {

        $transient = get_transient($transient_name);
        return $transient;

    } else {

        $response = wp_remote_get($url);
        $api_response = json_decode( wp_remote_retrieve_body( $response ), true );
        $values = $api_response['values'];

        if($values) {
            set_transient($transient_name, $values, $timeout);
            return $values;
        }        
    }
}