<?php

function get_primary_category($post_id = false) {
    if($post_id) {
        $article = $post_id;
    } else {
        $article = $post->ID;
    }
    $yoast_primary_term = yoast_get_primary_term_id('category', $article);
    $primary_category = get_term($yoast_primary_term, 'category');

    return $primary_category;
}

function the_primary_category($post_id = false) {
    $primary_category = get_primary_category($post_id);

    echo $primary_category->name;
}


function the_primary_category_link($post_id = false) {
    $primary_category = get_primary_category($post_id);

    $link = '<a href="' . get_term_link($primary_category->term_id , 'category') . '">' . $primary_category->name . '</a>';

    echo $link;
}