<?php

/*
    Register Blocks
*/


add_action('acf/init', 'my_register_blocks');
function my_register_blocks() {

    if( function_exists('acf_register_block_type') ) {

        acf_register_block_type(array(
            'name'              => 'top-stories-three-standard',
            'title'             => __('Top Stories (3, Standard)'),
            'description'       => __('Top stories block for three, with one primary and two secondardy.'),
            'render_template'   => 'blocks/top-stories-three-standard/top-stories-three-standard.php',
            'category'          => 'layout',
            'icon'              => '<svg height="202" viewBox="0 0 266 202" width="266" xmlns="http://www.w3.org/2000/svg"><g fill="#2a2a2a" fill-rule="evenodd"><path d="m0 0h123v202h-123z"/><path d="m143 0h123v92h-123z"/><path d="m143 110h123v92h-123z"/></g></svg>',
            'align'             => 'full',
        ));

    }
}

