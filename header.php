<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page" class="site">

	<nav class="banner-navigation grid">
		<div class="nav-wrapper">
			<?php get_template_part('template-parts/header/subscribe-btn'); ?>
	
			<?php get_template_part('template-parts/header/nameplate'); ?>

			<div class="utilities">
				<?php get_template_part('template-parts/header/social'); ?>

				<?php get_template_part('template-parts/header/hamburger'); ?>
			</div>
		</div>
	</nav>

	<div class="banner-bg"></div>

	<header class="site-header grid">		
		<?php get_template_part('template-parts/header/nameplate'); ?>
	</header>

	<?php get_template_part('template-parts/header/navigation'); ?>

	<main class="site-content">

	