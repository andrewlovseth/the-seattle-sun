<?php get_header(); ?>

<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

	<article>
        <?php the_post_thumbnail('large'); ?>
        <?php the_primary_category(); ?>
		<h2><?php the_title(); ?></h2>
		<?php the_content(); ?>
	</article>

<?php endwhile; endif; ?>

<?php get_footer(); ?>