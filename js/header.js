import './vendor/bling.js';

const banner = document.querySelector('.banner-navigation');
const nav = document.querySelector('.site-navigation');
const linkList = document.querySelector('.site-navigation .link-list');
const hamburger = document.querySelector('.js-nav-trigger');
const linkGroup = linkList.querySelector('ul');

function transitionIn() {
    document.body.classList.add('nav-overlay-open');
    banner.classList.add('fade-out');

    setTimeout(function () {
        nav.classList.add('slide-in');
        linkList.classList.add('slide-in');
    }, 1);

    setTimeout(function () {
        hamburger.classList.add('active');
    }, 50);

    setTimeout(function () {
        linkGroup.classList.add('show');
    }, 1200);
}

function transitionOut() {
    nav.classList.add('slide-out');
    linkList.classList.add('slide-out');
    linkGroup.classList.remove('show');

    setTimeout(function () {
        nav.classList.remove('slide-out', 'slide-in');
        linkList.classList.remove('slide-out', 'slide-in');
    }, 1200);

    setTimeout(function () {
        banner.classList.add('fade-in');
        hamburger.classList.remove('active');

        document.body.classList.remove('nav-overlay-open');
        banner.classList.remove('fade-out', 'fade-in');
    }, 1225);
}

const Header = {
    hamburger() {
        const hamburger = document.querySelector('.js-nav-trigger');

        hamburger.addEventListener('click', (e) => {
            let isActive = hamburger.classList.contains('active');

            if (isActive) {
                transitionOut();
            } else {
                transitionIn();
            }

            e.preventDefault();
        });
    },

    esc() {
        document.addEventListener('keyup', (e) => {
            if (e.key == 'Escape') {
                transitionOut();
            }
        });
    },

    belowTheFold() {
        const header = document.querySelector('.site-header');

        const observer = new IntersectionObserver(function (entries, observer) {
            entries.forEach((entry) => {
                banner.classList.toggle('scrolled', !entry.isIntersecting);
            });
        });

        observer.observe(header);
    },

    init: function () {
        this.hamburger();
        this.esc();
        this.belowTheFold();
    },
};

export default Header;
