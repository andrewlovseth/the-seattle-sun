import Header from './header.js';
import Animations from './animations.js';

(() => {
    Header.init();
    Animations.init();
})();
