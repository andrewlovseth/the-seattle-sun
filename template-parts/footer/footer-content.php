<?php
    $footer = get_field('footer', 'options');
    $copyright = $footer['copyright'];
    $upc_code = $footer['upc_code'];
    $cmyk = $footer['cmyk_registration_marks'];
?>

<div class="flex-container">
    <div class="copyright">
        <p><?php echo $copyright; ?></p>
    </div>

    <div class="badges">
        <div class="cmyk-registration-marks">
            <?php echo wp_get_attachment_image($cmyk['ID'], 'medium'); ?>
        </div>

        <div class="upc-code">
            <?php echo wp_get_attachment_image($upc_code['ID'], 'medium'); ?>
        </div>
    </div>
</div>