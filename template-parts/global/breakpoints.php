<?php

$args = wp_parse_args($args);
if(!empty($args)) {
    $breakpoints = $args['breakpoints']; 
    $selector = $args['selector']; 
}

if($breakpoints): ?>

    <style>
        <?php foreach($breakpoints as $breakpoint): ?>
            
            <?php
                $width = $breakpoint['breakpoint'];
                $positioning = $breakpoint['positioning'];
            ?>

            @media screen and (min-width: <?php echo $width; ?>px ) {
                <?php echo $selector; ?> .photo img {
                    object-position: <?php echo $positioning; ?>;
                }
            }

        <?php endforeach; ?>
    </style>

<?php endif; ?>