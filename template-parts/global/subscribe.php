<?php 

    $subscribe = get_field('subscribe', 'options');
    $headline = $subscribe['headline'];
    $copy = $subscribe['copy'];
    $photo = $subscribe['photo'];

?>

<section id="subscribe" class="subscribe">
    <div class="subscribe-wrapper">
        <div class="info">
            <div class="info-wrapper">
                <div class="headline">
                    <h3><?php echo $headline; ?></h3>
                </div>

                <div class="copy copy-3">
                    <?php echo $copy; ?>
                </div>

                <form class="subscribe-form">
                    <input type="email" placeholder="Email" class="email-field" />
                    <input type="submit" class="submit-btn" value="Subscribe" />
                </form>
            </div>
        </div>

        <div class="photo">
            <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
        </div>
    </div>
</section>

