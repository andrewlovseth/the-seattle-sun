<div class="nameplate">
    <?php $image = get_field('nameplate_logo', 'options'); if( $image ): ?>
        <a href="<?php echo site_url('/'); ?>">
            <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
        </a>
    <?php endif; ?>
</div>