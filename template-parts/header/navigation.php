<nav class="site-navigation">
    <div class="nav-content">

        <div class="recent">
            <h3>Recent Issues</h3>
            <?php
                $args = array(
                    'post_type' => 'issues',
                    'posts_per_page' => 3
                );
                $query = new WP_Query( $args );
                if ( $query->have_posts() ) : ?>

                <ul role="navigation">
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>

                        <?php
                            $title = get_the_title();
                            $prefix = 'Saturday, ';

                            if (substr($title, 0, strlen($prefix)) == $prefix) {
                                $title = substr($title, strlen($prefix));
                            } 

                        ?>

                        <li>
                            <a href="<?php the_permalink(); ?>"><?php echo $title; ?></a>
                        </li>

                    <?php endwhile; ?>
                </ul>
            <?php endif; wp_reset_postdata(); ?>
        </div>
    </div>

    <div class="link-list">

        <ul role="navigation">
            <li><a href="#">Home</a></li>
            <li><a href="#">Archives</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Contact</a></li>
        </ul>

    </div>
</nav>