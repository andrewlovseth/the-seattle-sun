<?php

/*

    Template Name: Home

*/

get_header(); ?>

    <?php
        $issue = get_field('active_issue');
        $args = ['issue' => $issue->ID];
        get_template_part('templates/saturday-sun', null, $args);
    ?>

<?php get_footer(); ?>