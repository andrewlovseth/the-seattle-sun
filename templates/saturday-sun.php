<?php 

    $args = wp_parse_args($args);
    if(!empty($args)) {
        $issue = $args['issue']; 
    }
    global $post;
    $post = get_post( $issue, OBJECT );
    setup_postdata($post);

?>

    <?php get_template_part('templates/saturday-sun/issue-header'); ?>

    <section class="news grid">

        <?php get_template_part('templates/saturday-sun/primary-story'); ?>

        <?php get_template_part('template-parts/global/triple-divider'); ?>

        <?php get_template_part('templates/saturday-sun/secondary-stories'); ?>

        <?php get_template_part('templates/saturday-sun/headlines'); ?>

    </section>

    <?php get_template_part('templates/saturday-sun/sports'); ?>

    <?php get_template_part('templates/saturday-sun/events'); ?>

<?php wp_reset_postdata(); ?>