<?php

    $events_post = get_field('events_post');
    if($events_post):

    $data = get_field('data', $events_post->ID);
    $sheet_id = $data['sheet_id'];
    $tab_id = $data['tab_id'];
    $slug = $data['slug'];
    $calendar_transient_name = 'calendar_' . $slug;

    if(get_transient($calendar_transient_name)) {
        $events = get_transient($calendar_transient_name);
    } else {
        $events = bearsmith_get_sheet_data($calendar_transient_name, $sheet_id, $tab_id, 2);
    }
?>

    <section class="events-calendar grid">

        <?php get_template_part('templates/saturday-sun/events/section-header'); ?>

        <?php if(have_rows('dates', $events_post->ID)): ?>

            <div class="days">

                <?php while(have_rows('dates', $events_post->ID)) : the_row(); ?>

                    <div class="day">
                        <?php get_template_part('templates/saturday-sun/events/day-header'); ?>

                        <?php
                            $args = ['events' => $events];
                            get_template_part('templates/saturday-sun/events/events-list', null, $args);
                        ?>
                    </div>

                
                <?php endwhile; ?>

            </div>
            
        <?php endif; ?>

        <?php get_template_part('templates/saturday-sun/events/suggest'); ?>

    </section>

<?php endif; ?>