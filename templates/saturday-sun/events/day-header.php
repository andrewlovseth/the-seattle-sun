<?php
 
    $date_string = get_sub_field('date');
    $date_object = DateTime::createFromFormat('Ymd', $date_string);
    $date = $date_object->format('l, F j');

?>

<div class="day-header">
    <?php get_template_part('template-parts/global/top-divider'); ?>
    <h3 class="section-header-title small"><?php echo $date; ?></h3>
    <?php get_template_part('template-parts/global/bottom-divider'); ?>
</div>