<?php

    $args = wp_parse_args($args);
    if(!empty($args)) {
        $events = $args['events']; 
    }

    $date_string = get_sub_field('date');
    $date_object = DateTime::createFromFormat('Ymd', $date_string);
    $this_date = $date_object->format('n/j');

    if($events):

?>

    <div class="events">
        <?php foreach($events as $event): ?>

            <?php
                $date = $event[0];
                $type = $event[1];
                $title = $event[2];
                $location = $event[3];
                $time = $event[4];
                $link = $event[5];
                $photo = $event[6];

                $event_class = 'event';
                
                if($photo) {
                    $event_class .= ' has-photo';
                }

                if($this_date === $date):

            ?>
        
                <div class="<?php echo $event_class; ?>">
                    <?php if($photo): ?>
                        <div class="photo">
                            <img src="<?php echo $photo; ?>" alt="<?php echo $title; ?>" />
                        </div>
                    <?php endif; ?>    
                    
                    <div class="info">
                        <div class="header">
                            <?php if($type): ?>
                                <span class="type"><?php echo $type; ?></span>
                            <?php endif; ?>

                            <?php if($title): ?>
                                <h4 class="title"><?php echo $title; ?></h4>
                            <?php endif; ?>
                        </div>

                        <div class="details">
                            <p>
                                <?php if($location): ?>
                                    <span class="location"><?php echo $location; ?></span>
                                <?php endif; ?>

                                <?php if($time): ?>
                                    <span class="time"><?php echo $time; ?></span>
                                <?php endif; ?>

                                <?php if($link):  ?>
                                    <span class="link">
                                        <a href="<?php echo esc_url($link); ?>" target="window">Event Info</a>
                                    </span>
                                <?php endif; ?>
                            </p>
                        </div>
                    </div>                                       
                </div>

            <?php endif; ?>

        <?php endforeach; ?>

    </div>

<?php endif; ?>