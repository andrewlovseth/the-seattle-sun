<?php

    $events = get_field('events', 'options');
    $section_header = $events['section_header'];

?>

<div class="events-section-header">
    <h2 class="events-section-header-title"><?php echo $section_header; ?></h2>
</div>