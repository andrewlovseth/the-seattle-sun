<?php

    $events = get_field('events', 'options');
    $suggest = $events['suggest'];
    $headline = $suggest['headline'];
    $copy = $suggest['copy'];
    $link = $suggest['cta'];

?>

<div class="suggest">
    <div class="headline">
        <h4><?php echo $headline; ?></h4>
    </div>

    <div class="copy copy-3">
        <?php echo $copy; ?>
    </div>

    <?php 
        if( $link ): 
        $link_url = $link['url'];
        $link_title = $link['title'];
        $link_target = $link['target'] ? $link['target'] : '_self';
    ?>

        <div class="cta">
            <a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
        </div>

    <?php endif; ?>

</div>