<?php

	$headlines = get_field('headlines');
	$section_header = get_field('headlines_section_header');

?>

<section class="headlines">

	<div class="section-header">
		<h2 class="section-header-title"><?php echo $section_header; ?></h2>
	</div>
	
	<?php if( $headlines ): ?>
		<?php foreach( $headlines as $headline ): ?>

			<article <?php post_class('headline'); ?>>
				<div class="content copy copy-2">
					<h4><?php echo get_the_title($headline->ID); ?>:</h4>
					<?php echo get_the_content( null, false, $headline->ID ); ?>
				</div>

				<div class="meta">
					<h5 class="topic no-border"><?php the_primary_category($headline->ID); ?></h5>
				</div>

			</article>

		<?php endforeach; ?>
	<?php endif; ?>

</section>