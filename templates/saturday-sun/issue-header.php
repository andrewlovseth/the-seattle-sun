<section class="issue-header grid">
    <?php get_template_part('template-parts/global/top-divider'); ?>

    <div class="flex-container">
        <div class="issue-no">
            <p>Issue No. <?php the_field('meta_issue_no'); ?></p>
        </div>

        <div class="title">
            <h2 class="section-header-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        </div>

        <div class="edition">
            <p><?php the_field('nameplate_tagline', 'options'); ?></p>
        </div>
    </div>

    <?php get_template_part('template-parts/global/bottom-divider'); ?>
</section>