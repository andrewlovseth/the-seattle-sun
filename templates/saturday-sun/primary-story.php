<?php

    $primary_story = get_field('primary_story');
    $template = $primary_story['template'];
    $story = $primary_story['post'];
    $breakpoints = $primary_story['photo_positioning'];
    $selector = '.primary-story';

    $post_class = 'primary-story';
    if($template) {
        $post_class .= ' template-' . $template;
    }

    if( $story ):
?>
    
    <?php
        $args = ['post_class' => $post_class, 'story' => $story];
        get_template_part('templates/saturday-sun/primary-story/story', null, $args);
    ?>      

    <?php
        $args = ['breakpoints' => $breakpoints, 'selector' => $selector];
        get_template_part('template-parts/global/breakpoints', null, $args);
    ?>    

<?php endif; ?>