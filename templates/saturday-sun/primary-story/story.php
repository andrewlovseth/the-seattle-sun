<?php

$args = wp_parse_args($args);
if(!empty($args)) {
    $post_class = $args['post_class']; 
    $story = $args['story']; 
}
?>

<article <?php post_class($post_class); ?>>
    <div class="article-header">
        <div class="meta">
            <h5 class="topic right"><?php the_primary_category($story->ID); ?></h5>
        </div>

        <div class="headline">
            <h1><?php echo get_the_title($story->ID); ?></h1>
        </div>
        
        <?php if(get_field('meta_dek', $story->ID)): ?>
            <div class="dek copy copy-1">
                <?php the_field('meta_dek', $story->ID); ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="photo">
        <div class="image">
            <?php echo get_the_post_thumbnail($story->ID, 'large'); ?>
        </div>

        <?php if(get_the_post_thumbnail_caption($story->ID)): ?>
            <div class="caption">
                <p><?php echo get_the_post_thumbnail_caption($story->ID); ?></p>
            </div>
        <?php endif; ?>
    </div>

    <div class="article-body copy copy-2">
        <?php echo get_the_content( null, false, $story->ID ); ?>
    </div>

</article>