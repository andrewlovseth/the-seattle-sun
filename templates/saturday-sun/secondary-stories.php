<section class="secondary-stories">

    <?php $secondary_stories = get_field('secondary_stories'); if($secondary_stories): ?>

        <?php $count = 1; foreach($secondary_stories as $secondary_story): ?>
            
            <?php
                $story = $secondary_story['post'];
                $template = $secondary_story['template'];
                $breakpoints = $secondary_story['photo_positioning'];
                $selector = '.secondary-story-' . $count;

                $post_class = 'secondary-story secondary-story-' . $count;

                if($template) {
                    $post_class .= ' template-' . $template;
                }
            ?>
            
            <?php
                $args = ['post_class' => $post_class, 'story' => $story];
                get_template_part('templates/saturday-sun/secondary-stories/story', null, $args);
            ?>      
            
            <?php get_template_part('template-parts/global/triple-divider'); ?>

            <?php
                $args = ['breakpoints' => $breakpoints, 'selector' => $selector];
                get_template_part('template-parts/global/breakpoints', null, $args);
            ?>       

        <?php $count++; endforeach; ?>

    <?php endif; ?>
    
    <?php get_template_part('template-parts/global/subscribe'); ?>
        
</section>
    