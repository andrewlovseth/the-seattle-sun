<?php

$args = wp_parse_args($args);
if(!empty($args)) {
    $post_class = $args['post_class']; 
    $story = $args['story']; 
}
?>

<article <?php post_class($post_class); ?>>
    <div class="photo">
        <div class="image">
            <?php echo get_the_post_thumbnail($story->ID, 'large'); ?>
        </div>

        <?php if(get_the_post_thumbnail_caption($story->ID)): ?>
            <div class="caption">
                <p><?php echo get_the_post_thumbnail_caption($story->ID); ?></p>
            </div>
        <?php endif; ?>
    </div>

    <div class="content">
        <div class="meta">
            <h5 class="topic"><?php the_primary_category($story->ID); ?></h5>
        </div>

        <div class="headline">
            <h3><?php echo get_the_title($story->ID); ?></h3>
        </div>

        <div class="copy copy-2">
            <?php echo get_the_content( null, false, $story->ID ); ?>
        </div>
    </div>
</article>