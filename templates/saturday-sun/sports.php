<?php

    $sports_post = get_field('sports_post');
    if($sports_post):

?>

    <section class="sports-page grid">

        <?php get_template_part('templates/saturday-sun/sports/section-header'); ?>

        <?php
            $template = get_field('template', $sports_post->ID);
            $args = ['template' => $template, 'sports-post' => $sports_post->ID];
            get_template_part('templates/saturday-sun/sports/' . $template, null, $args);
        ?>       

    </section>

<?php endif; ?>