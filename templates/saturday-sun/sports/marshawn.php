<?php

/*

    MARSHAWN!!!
    The Sports Page - A Summary Module Template on The Saturday Sun

*/

$args = wp_parse_args($args);
if(!empty($args)) {
    $template = $args['template']; 
    $sports_post = $args['sports-post']; 
}

if(have_rows('summary', $sports_post)): ?>

    <section class="sports-page-grid template-<?php echo $template; ?>">

        <?php while(have_rows('summary', $sports_post)) : the_row(); ?>

            <?php if( get_row_layout() == 'team' ): ?>

                <?php
                    $active = get_sub_field('active');
                    $summary = get_sub_field('summary');
                    $team_obj = get_sub_field('team');
                    $team = $team_obj['post'];
                    $record = $team_obj['record'];
                    $status = $team_obj['status'];
                    $meta = get_field('meta', $team->taxonomy . '_' . $team->term_id);
                    $logo = $meta['logo'];
                    $name = $meta['name'];
                    $sport = $meta['sport'];

                    $team_class = 'team';

                    if($name) {
                        $team_class .= ' team-' . sanitize_title_with_dashes($name);
                    }
                    
                    if($active == TRUE) {
                        $team_class .= ' active';
                    } else {
                        $team_class .= ' inactive';
                    }
                ?>

                <div class="<?php echo $team_class; ?>">
                    <div class="team-header">
                        <div class="team-logo">
                            <?php echo wp_get_attachment_image($logo['ID'], 'full'); ?>
                        </div>

                        <div class="team-info">
                            <h3>
                                <?php echo $name; ?>
                                <?php if($status): ?>
                                    <span class="record">(<?php echo $record; ?>)</span>
                                <?php endif; ?>
                            </h3>
                            <?php if($status): ?>
                                <p class="status"><?php echo $status; ?></p>
                            <?php endif; ?>
                        </div>                        
                    </div>

                    <div class="copy copy-2">
                        <?php echo $summary; ?>
                    </div>

                    <?php 
                        $args = ['team' => $team, 'sport' => $sport];
                        get_template_part('templates/saturday-sun/sports/scores', null, $args);
                    ?>
                    
                    <?php 
                        $args = ['team' => $team, 'sport' => $sport];
                        get_template_part('templates/saturday-sun/sports/schedule', null, $args);
                    ?>

                </div>
                    
            <?php endif; ?>

        <?php endwhile; ?>

    </section>

<?php endif; ?>