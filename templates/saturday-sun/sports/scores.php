<?php 

$args = wp_parse_args($args);
if(!empty($args)) {
    $team = $args['team']; 
    $sport = $args['sport']; 
}

$header = get_sub_field('scores_header');

if(have_rows('scores')): ?>

    <div class="scores">
        <div class="scores-header">
            <h5><?php echo $header; ?></h5>
        </div>

        <?php while(have_rows('scores')): the_row(); ?>

            <?php
                // Sub Fields
                $date = get_sub_field('date');
                $round = get_sub_field('round');
                $seattle = get_sub_field('seattle');
                $opponent = get_sub_field('opponent');

                // Seattle
                $home_away = $seattle['home_away'];
                $score = $seattle['score'];
                $meta = get_field('meta', $team->taxonomy . '_' . $team->term_id);
                $logo = $meta['logo'];
                $abbr = $meta['abbreviation'];                

                // Opponent
                $opponent_team = $opponent['team'];
                $opponent_score = $opponent['score'];
                $opponent_meta = get_field('meta', $opponent_team->taxonomy . '_' . $opponent_team->term_id);
                $opponent_logo = $opponent_meta['logo'];
                $opponent_abbr = $opponent_meta['abbreviation'];

                // Game Class
                $game_class = 'game';

                if($home_away) {
                    $game_class .= ' ' . $home_away;
                }       

                if($sport) {
                    $game_class .= ' ' . sanitize_title_with_dashes($sport);
                }

            ?>
        
            <div class="<?php echo $game_class; ?>">
                <div class="game-info">
                    <div class="date">
                        <span><?php echo $date; ?></span>
                    </div>
                </div>

                <div class="score-team seattle">
                    <div class="info">
                        <div class="logo">
                            <?php echo wp_get_attachment_image($logo['ID'], 'full'); ?>
                        </div>

                        <div class="abbr">
                            <span><?php echo $abbr; ?></span>
                        </div>
                    </div>

                    <div class="score">
                        <span><?php echo $score; ?></span>                        
                    </div>
                </div>

                <div class="score-team opponent">
                    <div class="info">
                        <div class="logo">
                            <?php echo wp_get_attachment_image($opponent_logo['ID'], 'full'); ?>
                        </div>

                        <div class="abbr">
                            <span><?php echo $opponent_abbr; ?></span>
                        </div>
                    </div>

                    <div class="score">
                        <span><?php echo $opponent_score; ?></span>                        
                    </div>
                </div>    
            </div>

        <?php endwhile; ?>
    </div>
<?php endif; ?>           