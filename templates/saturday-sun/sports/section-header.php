<?php

    $sports = get_field('sports', 'options');
    $section_header = $sports['section_header'];

?>

<div class="section-header">
    <h2 class="section-header-title"><?php echo $section_header; ?></h2>
</div>